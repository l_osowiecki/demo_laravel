<?php

namespace App\Listeners;

use App\Events\PushInformation;
use Illuminate\Support\Facades\Mail;
use App\Mail\XmlGenerated;

class MailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PushInformation  $event
     * @return void
     */
    public function handle(PushInformation $event)
    {
        Mail::to($event->receiver)
            ->send(new XmlGenerated($event->xmlData));
    }
}
