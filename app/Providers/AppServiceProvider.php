<?php

namespace App\Providers;

use App\Contracts\Builders\XmlDataContract;
use App\Contracts\Repositories\ArticlesRepositoryContract;
use App\Models\Articles;
use App\Contracts\Models\ArticleContract;
use App\Builders\XmlNativeBuilder;
use App\Repositories\ArticlesRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ArticleContract::class, Articles::class);
        $this->app->bind(ArticlesRepositoryContract::class, ArticlesRepository::class);
        $this->app->bind(XmlDataContract::class, XmlNativeBuilder::class);
    }
}
