<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class XmlGenerated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var string Xml data
     */
    private $attachment;

    /**
     * Create a new message instance.
     *
     * @param $attachment
     *
     * @return void
     */
    public function __construct(string $attachment)
    {
        $this->attachment = $attachment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.xml-generated')
                    ->attachData($this->attachment, 'data.xml', [
                        'mime' => 'text/xml'
                    ]);
    }
}
