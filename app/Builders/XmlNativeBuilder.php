<?php
namespace App\Builders;

use App\Contracts\Repositories\ArticlesRepositoryContract;
use \DOMDocument;
use App\Contracts\Builders\XmlDataContract;

class XmlNativeBuilder implements XmlDataContract {

    private $xmlDoc;

    public function __construct()
    {
        $this->xmlDoc = new DOMDocument();
    }

    /**
     * @param ArticlesRepositoryContract $data
     *
     * @return string
     */
    public function build(ArticlesRepositoryContract $data): string
    {
        $base = $this->xmlDoc->appendChild(
            $this->xmlDoc->createElement('items')
        );

        foreach ($data->all() as $key => $item) {
            $xmlItem = $base->appendChild($this->xmlDoc->createElement('item'));
            foreach ($item->toArray() as $key => $property) {
                $xmlItem->appendChild($this->xmlDoc->createElement($key, $property));
            }
        }

        $this->xmlDoc->formatOutput = true;

        return $this->xmlDoc->saveXML();
    }
}