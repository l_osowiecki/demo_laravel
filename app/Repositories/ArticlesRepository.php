<?php

namespace App\Repositories;

use App\Contracts\Models\ArticleContract;
use App\Contracts\Repositories\ArticlesRepositoryContract;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class ArticlesRepository.
 *
 * @package namespace App\Repositories;
 */
class ArticlesRepository extends BaseRepository implements ArticlesRepositoryContract
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ArticleContract::class;
    }

    public function fetch(array $criterias)
    {
        foreach ($criterias as $criteria) {
            $this->pushCriteria($criteria);
        }
    }
}
