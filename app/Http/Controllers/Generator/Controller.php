<?php

namespace App\Http\Controllers\Generator;

use App\Contracts\Builders\XmlDataContract;
use App\Criteria\CriteriaFactory;
use App\Events\PushInformation;
use App\Http\Controllers\Controller as BaseController;
use App\Http\Requests\GenerateXmlRequest;
use App\Contracts\Repositories\ArticlesRepositoryContract;

class Controller extends BaseController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('welcome');
    }

    /**
     * @param GenerateXmlRequest $request
     * @param ArticlesRepositoryContract $repository
     * @param XmlDataContract $xmlBuilder
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function generate(
        GenerateXmlRequest $request,
        ArticlesRepositoryContract $repository,
        XmlDataContract $xmlBuilder
    )
    {
        $repository->fetch(
            CriteriaFactory::fetchCriteria($request->input('source'))
        );

        event(new PushInformation(
            $request->input('email'),
            $xmlBuilder->build($repository)
        ));

        session()->flash('success', true);

        return redirect()->back();
    }
}
