<?php
namespace App\Criteria;

class CriteriaFactory {
    public static function fetchCriteria(string $source): array
    {
        switch ($source) {
            case 'lime':
                return [
                    new ColorCriteria($source)
                ];
            case 'popular_lime':
                return [
                    new ColorCriteria('lime'),
                    new PopularCriteria()
                ];
            case 'en_and_more_than_5':
                return [
                    new LanguageCriteria('en'),
                    new ViewsCriteria('>', 5)
                ];
            case 'article_slug':
                return [
                    new ArticlesSlugCriteria()
                ];
        };
    }
}