<?php

namespace App\Criteria;

use Illuminate\Support\Facades\DB;
use App\Contracts\Models\ArticleContract;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ArticlesSlugCriteria.
 *
 * @package namespace App\Criteria;
 */
class ArticlesSlugCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->select('id', DB::raw('replace(lower(title), " ", "-") as slug'))->take(10);
    }
}
