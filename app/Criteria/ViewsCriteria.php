<?php

namespace App\Criteria;

use App\Contracts\Models\ArticleContract;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class EnglishAndMoreThanFiveCriteria.
 *
 * @package namespace App\Criteria;
 */
class ViewsCriteria implements CriteriaInterface
{
    private $operator;
    private $value;

    public function __construct(string $operator, int $value)
    {
        $this->operator = $operator;
        $this->value = $value;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where(ArticleContract::A_VIEWS, $this->operator, $this->value);
    }
}
