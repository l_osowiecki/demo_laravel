<?php

namespace App\Criteria;

use App\Contracts\Models\ArticleContract;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ColorCriteria.
 *
 * @package namespace App\Criteria;
 */
class ColorCriteria implements CriteriaInterface
{
    private $color;

    public function __construct(string $color)
    {
        $this->color = $color;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where(ArticleContract::A_COLOR, $this->color);
    }
}
