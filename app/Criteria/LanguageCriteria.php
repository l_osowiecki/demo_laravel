<?php

namespace App\Criteria;

use App\Contracts\Models\ArticleContract;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class LanguageCriteria.
 *
 * @package namespace App\Criteria;
 */
class LanguageCriteria implements CriteriaInterface
{
    private $language;

    public function __construct(string $language)
    {
        $this->language = $language;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where(ArticleContract::A_LANGUAGE, $this->language);
    }
}
