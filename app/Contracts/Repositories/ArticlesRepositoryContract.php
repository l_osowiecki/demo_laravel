<?php

namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ArticlesRepositoryContract.
 *
 * @package namespace App\Repositories;
 */
interface ArticlesRepositoryContract extends RepositoryInterface
{
    /**
     * @param array $criteria
     *
     * @return void
     */
    public function fetch(array $criteria);
}
