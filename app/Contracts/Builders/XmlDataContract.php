<?php
namespace App\Contracts\Builders;

use App\Contracts\Repositories\ArticlesRepositoryContract;

/**
 * Interface XmlDataContract
 * @package App\Contracts\Presenter
 */
interface XmlDataContract {

    /**
     * @param ArticlesRepositoryContract $repository
     *
     * @return string
     */
    public function build(ArticlesRepositoryContract $repository): string;
}