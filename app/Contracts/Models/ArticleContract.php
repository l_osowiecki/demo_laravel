<?php

namespace App\Contracts\Models;

interface ArticleContract {
    /**
     * Title field name
     */
    const A_TITLE = 'title';

    /**
     * Description field name
     */
    const A_DESCRIPTION = 'description';

    /**
     * View field name
     */
    const A_VIEWS = 'views';

    /**
     * Background color field name
     */
    const A_COLOR = 'color';

    /**
     * Article language field name
     */
    const A_LANGUAGE = 'language';
}