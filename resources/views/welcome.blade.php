<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="/css/app.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>
                <div class="content__form">
                    @if (Route::has('generator'))
                        @if (session()->get('success'))
                            <div class="alert alert-success" role="alert">
                                Xml has been send
                            </div>
                        @endif
                        {{ Form::open(['route' => 'generator', 'method' => 'post']) }}
                            <div class="form-group">
                                {{ Form::label('email', 'Please, provide your email', ['class' => 'control-label']) }}
                                {{ Form::email('email', $value ?? '', ['class' => $errors->has('email') ? 'form-control is-invalid' : 'form-control']) }}
                                @if ($errors->has('email'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                                {{ Form::label('source', 'Data type', ['class' => 'control-label']) }}
                                {{ Form::select('source', [
                                        'lime' => 'Articles with color set to lime',
                                        'popular_lime' => 'Top 10 articles with lime colors',
                                        'en_and_more_than_5' => 'Articles in english with views more than 5',
                                        'article_slug' => 'Get 10 articles with slug',
                                    ], 'lime')
                                }}
                            </div>
                            {{ Form::submit('Send xml!') }}
                        {{ Form::close() }}
                    @endif
                </div>
            </div>
        </div>
    </body>
</html>
