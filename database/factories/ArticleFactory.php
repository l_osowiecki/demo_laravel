<?php

use Faker\Generator as Faker;
use App\Models\Articles;
use App\Contracts\Models\ArticleContract;

$factory->define(Articles::class, function (Faker $faker) {
    return [
        ArticleContract::A_TITLE => $faker->sentence,
        ArticleContract::A_DESCRIPTION => $faker->text,
        ArticleContract::A_VIEWS => $faker->randomDigitNotNull,
        ArticleContract::A_COLOR => $faker->safeColorName,
        ArticleContract::A_LANGUAGE => $faker->languageCode
    ];
});
